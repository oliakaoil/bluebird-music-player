
package com.bluebird;

import java.util.List;
import android.content.Context;
import android.view.ViewGroup;
import android.view.View;

import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.graphics.Color;
import android.util.TypedValue;

public class AudioFileAdapter extends BaseAdapter {
  private Context mContext;
  private List<dbmAudioFile> mFiles;

  public AudioFileAdapter(Context c , List<dbmAudioFile> files) 
  {
    mContext = c;
    mFiles = files;
  }

  public int getCount() 
  {
    return mFiles.size();
  }

  public Object getItem(int position) 
  {
    return null;
  }

  public long getItemId(int position) 
  {
    return 0;
  }

  // create a new TextView for each item referenced by the Adapter
  public View getView(int position, View convertView, ViewGroup parent) 
  {
    TableLayout tbl_layout;
    TableRow tbl_row1;
    TableRow tbl_row2;
    TextView filename_txt;
    TextView filetype_txt;
      
    if (convertView == null) 
    {
      tbl_layout = new TableLayout(mContext);
      
      tbl_row1 = new TableRow(mContext);
      tbl_row1.setLayoutParams(new LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT ));
      tbl_row1.setOrientation( 0 );
      
      tbl_row2 = new TableRow(mContext);
      tbl_row2.setLayoutParams(new LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT ));
      tbl_row2.setOrientation( 0 );
        
      filename_txt = new TextView(mContext);
      filename_txt.setTextColor(Color.parseColor("#ffffff"));
      filename_txt.setTextSize( android.util.TypedValue.COMPLEX_UNIT_DIP , 17 );
      //filename_txt.setLayoutParams( new LayoutParams( LayoutParams.FILL_PARENT , LayoutParams.FILL_PARENT ) );
        
      filetype_txt = new TextView(mContext);
      filetype_txt.setTextColor(Color.parseColor("#c7c7c7"));
      filetype_txt.setTextSize( android.util.TypedValue.COMPLEX_UNIT_DIP , 10 );
      filetype_txt.setPadding( 3 , 0 , 0 , 0 );
      //filetype_txt.setGravity( android.view.Gravity.BOTTOM );
      //filetype_txt.setLayoutParams( new LayoutParams( LayoutParams.FILL_PARENT , LayoutParams.FILL_PARENT ) );
        
      tbl_row1.addView( filename_txt );
      tbl_row2.addView( filetype_txt );
        
      tbl_layout.setLayoutParams(new GridView.LayoutParams( android.view.ViewGroup.LayoutParams.FILL_PARENT , 65 ));
      tbl_layout.setPadding(8, 8, 8, 8);
      tbl_layout.addView(tbl_row1, new TableLayout.LayoutParams( LayoutParams.FILL_PARENT , LayoutParams.WRAP_CONTENT));
      tbl_layout.addView(tbl_row2, new TableLayout.LayoutParams( LayoutParams.FILL_PARENT , LayoutParams.WRAP_CONTENT));
       
    } else {
      tbl_layout = (TableLayout) convertView;
      tbl_row1 = (TableRow)tbl_layout.getChildAt(0);
      tbl_row2 = (TableRow)tbl_layout.getChildAt(1);
      
      filename_txt = (TextView)tbl_row1.getChildAt(0);
      filetype_txt = (TextView)tbl_row2.getChildAt(0);
    }

    dbmAudioFile af = mFiles.get( position );
    String filepath = af.getFilePath();
    Integer last_slash = filepath.lastIndexOf("/");
    Integer last_dot = filepath.lastIndexOf(".");
    String filename = filepath.substring( last_slash + 1 );
    String no_ext = filename.substring( 0 , last_dot - 1 );
    String ext = filename.substring( last_dot );
    
    filename_txt.setText( no_ext );
    filetype_txt.setText( ext.toLowerCase() );
    return tbl_layout;
  }
}