
package com.bluebird;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.AudioManager;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.bluebird.R;

public class dbmAudioService extends Service implements
  OnPreparedListener,
  OnBufferingUpdateListener,
// OnInfoListener,
  OnCompletionListener, 
  OnErrorListener
  {

  final static private String LOG_TAG = "dbmAudioService";
  private static final int NOTIF_ID = 1;
  private int startId;
  public MediaPlayer mplayer;
  final static private String[] supported_exts = {"aac" , "mp3" , "3gp" , "mp4" , "m4a" , "flac" , "ogg" , "mkv" , "wav"};
  
  private Looper serviceLooper;
  private ServiceHandler serviceHandler;
  private NotificationManager notifManager;
  private TelephonyManager telephonyManager;
  private PhoneStateListener listener;
  private boolean isPausedInCall = false;
  
  private static final String SERVICE_PREFIX = "com.bluebird.";
  public static final String UPDATE_TRACK_COMPLETE = SERVICE_PREFIX + "TrackComplete";
  public static final String UPDATE_DIE = SERVICE_PREFIX + "ServiceClose";
  public static final String UPDATE_TRACK_PREPARE = SERVICE_PREFIX + "TrackPrepare";
  public static final String UPDATE_PLAYER_ERROR = SERVICE_PREFIX + "PlayerError";
  final static public String ACTION_STOP = SERVICE_PREFIX + "Stop";    
  final static public String ACTION_PLAYPAUSE = SERVICE_PREFIX + "PlayPause";
  final static public String ACTION_PAUSE = SERVICE_PREFIX + "Pause";
  final static public String ACTION_START = SERVICE_PREFIX + "Start";
  final static public String ACTION_PAUSED_STOP = SERVICE_PREFIX + "PausedStop";
  
  static final String ACTION_FOREGROUND = SERVICE_PREFIX + "FOREGROUND";
  static final String ACTION_BACKGROUND = SERVICE_PREFIX + "BACKGROUND";
  private static final Class<?>[] mSetForegroundSignature = new Class[] { boolean.class };
  private static final Class<?>[] mStartForegroundSignature = new Class[] { int.class, Notification.class };
  private static final Class<?>[] mStopForegroundSignature = new Class[] { boolean.class };

  private NotificationManager mNM;
  private Method mSetForeground;
  private Method mStartForeground;
  private Method mStopForeground;
  private Object[] mSetForegroundArgs = new Object[1];
  private Object[] mStartForegroundArgs = new Object[2];
  private Object[] mStopForegroundArgs = new Object[1];  

  private final class ServiceHandler extends Handler {
    public ServiceHandler(Looper looper) {
      super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
      startId = msg.arg1;
      onHandleIntent((Intent) msg.obj);
    }
  }  
  
  
  /*
   * Service Overrides
   */
  
  @Override
  public void onCreate() 
  {
    Log.e( LOG_TAG , "dbmAudioService onCreate");
    super.onCreate();
    
    mplayer = new MediaPlayer();
    mplayer.setOnBufferingUpdateListener(this);
    mplayer.setOnCompletionListener(this);
    mplayer.setOnErrorListener(this);
    //mplayer.setOnInfoListener(this);
    mplayer.setOnPreparedListener(this);
    
    notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    // pause when the phone rings, etc.
    _watch_telephony();
    
    // run in the foreground
    initForeground();
    
    // run in a different thread
    HandlerThread thread = new HandlerThread("dbmAudioService:WorkerThread");
    thread.start();
    serviceLooper = thread.getLooper();
    serviceHandler = new ServiceHandler(serviceLooper);    
  }
  
  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Log.e( LOG_TAG , "dbmAudioService onStartCommand" );
    handleCommand(intent);
    
    Message message = serviceHandler.obtainMessage();
    message.arg1 = startId;
    message.obj = intent;
    serviceHandler.sendMessage(message);    
    
    // continue running until explicitly stopped
    return START_STICKY;
  }  
  
  protected void onHandleIntent(Intent intent) 
  {
    Log.e( LOG_TAG , "onHandleIntent");
    
    if( intent == null )
      return;
    
    String action = intent.getAction();
    String media_url = "";
    
    if( action == null )
      return;    
    
    Log.e( LOG_TAG , "onHandleIntent: " + action );
    
    if( intent.hasExtra("com.bluebird.af") )
      media_url = (String) intent.getStringExtra("com.bluebird.af");
    
    if( action.equals(ACTION_START) || action.equals(ACTION_PLAYPAUSE) 
         || action.equals(ACTION_STOP) || action.equals(ACTION_PAUSE) )
    {
      clicker( action , media_url );
      return;
    }
    
    if( action.equals(ACTION_PAUSED_STOP) && !mplayer.isPlaying() )
    {
      clicker( ACTION_STOP , "" );
      stopService(new Intent(this,dbmAudioService.class));
      return;
    }
  }
  
  @Override
  public IBinder onBind(Intent intent) 
  {
    return null;
  }    
  
  @Override
  public void onStart(Intent intent, int startId) 
  {
    Log.e( LOG_TAG , "dbmAudioService onStart");
    handleCommand(intent);
  }  
  
  @Override
  public void onDestroy() 
  {
    super.onDestroy();
    Log.e( LOG_TAG , "dbmAudioService onDestroy");
    
    synchronized (this) {
      if (mplayer != null) {
        try {
          mplayer.stop();
        }catch(Exception e){
        }        
        mplayer.release();
        mplayer = null;
      }
    }

    stopForegroundCompat(R.string.foreground_service_started);
    serviceLooper.quit();

    notifManager.cancel(NOTIF_ID);
    
    //if (lastChangeBroadcast != null)
    //  getApplicationContext().removeStickyBroadcast(lastChangeBroadcast);
    
    updateBroadcast(UPDATE_DIE);

    telephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
  }  
  
  /*
   * MediaPlayer overrides
   */
  
  @Override
  public void onBufferingUpdate(MediaPlayer mp, int progress) {
    //if( progress )
    //
  }  
  
  @Override
  public void onCompletion(MediaPlayer mp) {  
    Log.e( LOG_TAG ,"completed, next track");
    updateBroadcast( UPDATE_TRACK_COMPLETE );
  }
  
  @Override
  public void onPrepared(MediaPlayer mp) {
    Log.e( LOG_TAG ,"prepared,starting playback");
    updateBroadcast( UPDATE_TRACK_PREPARE );
    mp.start();
  }  
 
  @Override
  public boolean onError(MediaPlayer mp, int what, int extra) {
    mp.reset();

    String msg = "unrecognized error";
    
    if( what == MediaPlayer.MEDIA_ERROR_SERVER_DIED )
      msg = "server died";
    
    if( what == MediaPlayer.MEDIA_ERROR_UNKNOWN )
      msg = "uknown error";
    
    Log.e( LOG_TAG , "Error: " + msg + " extra=" + String.valueOf(extra) );
    
    updateBroadcast( UPDATE_PLAYER_ERROR );
    
    return true;
  }
    
  public void clicker( String act , String media_url )
  {
    Log.e( LOG_TAG ,"clicker: " + act);
    
    if( act.equals(ACTION_PAUSE) )
    {
      if( mplayer.isPlaying() )
        mplayer.pause();
      
      return;
    }
    
    if( act.equals(ACTION_STOP) )
    {
      try {
        mplayer.stop();
      } catch( Exception e ){
        Log.e( LOG_TAG ,"stop failed",e);  
      }
      
      mplayer.reset();
      return;
    }
        
    if( act.equals(ACTION_PLAYPAUSE) )
    {   
      if( mplayer.isPlaying() )
      {
        mplayer.pause();
        updateBroadcast( ACTION_PAUSE );
      } else {
        mplayer.start();
        updateBroadcast( ACTION_START );
      }
      
      return;
    }
    
    if( act.equals(ACTION_START) )
    {
      try {
        mplayer.stop();
        mplayer.reset();
      } catch( Exception e ){
        Log.e( LOG_TAG ,"stop/reset for play failed",e);  
      }      
      
      Log.e( LOG_TAG ,"loading playback for " + media_url );
      
      try {
        mplayer.setDataSource( media_url );
        mplayer.setAudioStreamType( AudioManager.STREAM_MUSIC );
      } catch (Exception e) {
        Log.e( LOG_TAG ,"setDataSource error",e);
      }
      
      try {
        // mplayer.prepare(); mplayer.start();
        mplayer.prepareAsync();
      } catch (Exception e) {
        Log.e( LOG_TAG ,"prepare error",e);
      }
      
      return;
    }
  }  
  
  public static boolean file_supported( String filename )
  {
    //supported_exts
    int dotposition= filename.lastIndexOf(".");
    String ext = filename.substring(dotposition + 1, filename.length()).toLowerCase();
    
    List<String> extList = Arrays.asList(supported_exts);
    
    return extList.contains( ext );
  }

  private void initForeground()
  {
    Log.e( LOG_TAG , "initForeground" );
    
    mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    try {
        mStartForeground = getClass().getMethod("startForeground", mStartForegroundSignature);
        mStopForeground = getClass().getMethod("stopForeground", mStopForegroundSignature);
        return;
    } catch (NoSuchMethodException e) {
        // Running on an older platform.
        mStartForeground = mStopForeground = null;
    }
    
    try {
      mSetForeground = getClass().getMethod("setForeground", mSetForegroundSignature);
    } catch (NoSuchMethodException e) {
        throw new IllegalStateException("OS doesn't have Service.startForeground OR Service.setForeground!");
    }    
  }
  
  private void startForegroundCompat(int id, Notification notification) 
  {
    Log.e( LOG_TAG , "startForegroundCompat" );
    
    // If we have the new startForeground API, then use it.
    if (mStartForeground != null) 
    {
        mStartForegroundArgs[0] = Integer.valueOf(id);
        mStartForegroundArgs[1] = notification;
        invokeMethod(mStartForeground, mStartForegroundArgs);
        return;
    }

    // Fall back on the old API.
    mSetForegroundArgs[0] = Boolean.TRUE;
    invokeMethod(mSetForeground, mSetForegroundArgs);
    mNM.notify(id, notification);
  }

  private void stopForegroundCompat(int id) {
    // If we have the new stopForeground API, then use it.
    if (mStopForeground != null) {
        mStopForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(mStopForeground, mStopForegroundArgs);
        return;
    }

    // Fall back on the old API.  Note to cancel BEFORE changing the
    // foreground state, since we could be killed at that point.
    mNM.cancel(id);
    mSetForegroundArgs[0] = Boolean.FALSE;
    invokeMethod(mSetForeground, mSetForegroundArgs);
  }
  
  private void invokeMethod(Method method, Object[] args) {
    try {
      method.invoke(this, args);
    } catch (InvocationTargetException e) {
      Log.w( LOG_TAG , "01 Unable to invoke method", e);
    } catch (IllegalAccessException e) {
      Log.w( LOG_TAG , "02 Unable to invoke method", e);
    }
  }
  
  private void handleCommand(Intent intent) 
  {
    Log.e( LOG_TAG , "handleCommand | " + String.valueOf(intent.getAction()) );
    
    if (ACTION_FOREGROUND.equals(intent.getAction())) 
    {
      Log.e( LOG_TAG , "starting foreground service" );
      
      CharSequence text = getText(R.string.foreground_service_started);

      // Set the icon, scrolling text and timestamp
      Notification notification = new Notification( R.drawable.icon, text, System.currentTimeMillis() );

      // The PendingIntent to launch our activity if the user selects this notification
      PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, audioPlayer.class), 0);

      // Set the info for the views that show in the notification panel.
      notification.setLatestEventInfo(this, getText(R.string.local_service_label), text, contentIntent);

      startForegroundCompat(R.string.foreground_service_started, notification);

    } else if (ACTION_BACKGROUND.equals(intent.getAction())) {
        stopForegroundCompat(R.string.foreground_service_started);
    }
  }  
  
  private void updateBroadcast( String update_name )
  {
    Log.e( LOG_TAG , "updateBroadcast | " + update_name );
    
    try {
      Intent tempUpdateBroadcast = new Intent( update_name );
      getApplicationContext().sendBroadcast(tempUpdateBroadcast);
    }catch(Exception e){
      Log.e( LOG_TAG , "Broadcast error",e);
    }
  }
  
  private void _watch_telephony()
  {
    telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    // Create a PhoneStateListener to watch for off-hook and idle events
    listener = new PhoneStateListener() {
      @Override
      public void onCallStateChanged(int state, String incomingNumber) {
        switch (state) {
          case TelephonyManager.CALL_STATE_OFFHOOK:
          case TelephonyManager.CALL_STATE_RINGING:
            // Phone going off-hook or ringing, pause the player.
            if (mplayer.isPlaying()) {
              clicker( ACTION_PLAYPAUSE , null );
              updateBroadcast( ACTION_PAUSE );
              isPausedInCall = true;
            }
          break;
          case TelephonyManager.CALL_STATE_IDLE:
            if (isPausedInCall) 
            {
              isPausedInCall = false;
              clicker( ACTION_PLAYPAUSE , null );
              updateBroadcast( ACTION_START );
            }
          break;
        }
      }
    };

    // Register the listener with the telephony manager.
    telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);      
  }
}