
package com.bluebird;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;

import android.net.ConnectivityManager;
import android.content.Context;
import android.net.NetworkInfo;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.bluebird.R;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.DropboxAPI.DropboxLink;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;
import com.dropbox.client2.session.TokenPair;
import com.dropbox.client2.exception.DropboxException;


public class audioPlayer extends Activity
{
	DropboxAPI<AndroidAuthSession> mApi;
	final static private String LOG_TAG = "Db sys";
	final static private String ACCOUNT_PREFS_NAME = "prefs";
  final static private String ACCESS_KEY_NAME = "ACCESS_KEY";
  final static private String ACCESS_SECRET_NAME = "ACCESS_SECRET";
  final static private String APP_KEY = "mg63s8y24wp9aox";
  final static private String APP_SECRET = "qnazxs3pf738r58";    
  private static final int OPT_SIGN_OUT = 1;
  private static final int OPT_REFRESH_LIST = 2;
    
  // If you'd like to change the access type to the full Dropbox
  final static private AccessType ACCESS_TYPE = AccessType.APP_FOLDER;
  private List<dbmAudioFile> audio_files;
  private Integer track_index = -1;
  private TextView ct;
  private GridView audio_grid;
  private dbmIntentReceiver dbmRadio;
  private boolean playback_shuffle;
  private List<dbmAudioFile> audio_files_shuffled;
	
  protected void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.audio_player);
    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    
    ct = (TextView)findViewById(R.id.currentTrack);
	audio_grid = (GridView)findViewById(R.id.gridView1);
	audio_files = new ArrayList<dbmAudioFile>();
	audio_files_shuffled = new ArrayList<dbmAudioFile>();
	playback_shuffle = false;

	// load the stored session
    String[] stored = getKeys();
    if (stored != null) 
    {
      AppKeyPair appKeyPair = new AppKeyPair(APP_KEY.replace("8","1"), APP_SECRET.replace("8","1"));
      AccessTokenPair accessToken = new AccessTokenPair(stored[0], stored[1]);
      AndroidAuthSession session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE, accessToken);
      mApi = new DropboxAPI<AndroidAuthSession>(session);
      mApi.getSession().setAccessTokenPair(accessToken);        
    } else {
      // back to home
      Intent loginIntent = new Intent( audioPlayer.this , BlueBird.class );
      startActivityForResult( loginIntent , 0 );          
    }
    
    if (savedInstanceState != null) {
      // load audio files list
    }
    
    // register a broadcast receiver for the primary audio service
    _load_receiver();
    
    // start the primary audio service
    Intent dbmAudioServiceIntent = new Intent(this, dbmAudioService.class);
    startService(dbmAudioServiceIntent);
            
    // show the list if its available, and update it anyways, or load from API
    _load_list();
    
    _enable_controls();
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    super.onCreateOptionsMenu(menu);
    
    menu.add(0, OPT_REFRESH_LIST, 0, R.string.opt_menu_refresh_list).setIcon(android.R.drawable.ic_menu_rotate);
    menu.add(0, OPT_SIGN_OUT, 0, R.string.opt_menu_signout).setIcon(android.R.drawable.ic_menu_close_clear_cancel);

    return true;
  } 
  
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case OPT_SIGN_OUT:
        stopService(new Intent(this,dbmAudioService.class));
        signOut();
        super.onDestroy();
        this.finish();
      break;
      case OPT_REFRESH_LIST:
        _load_list();
      break;
    }

    return true;
  }  
  
    
  @Override
  protected void onResume() {
    super.onResume();    
  }

  @Override
  protected void onStop() {
    super.onStop();
    
    // destroy the audio service if paused
    Intent dbmAudioServiceIntent = new Intent( this , dbmAudioService.class );
    dbmAudioServiceIntent.setAction(dbmAudioService.ACTION_PAUSED_STOP);
    startService(dbmAudioServiceIntent);    
  }

  @Override
  protected void onPause() {
    super.onPause();
  }  
  
  protected void onDestroy() {
    super.onDestroy();
  }  
 
  private void _enable_controls()
  {
    ImageButton playBtn = (ImageButton)findViewById(R.id.playpauseBtn);
    ImageButton nextBtn = (ImageButton)findViewById(R.id.nextBtn);
    ImageButton prevBtn = (ImageButton)findViewById(R.id.previousBtn);
    ImageButton shuffleBtn = (ImageButton)findViewById(R.id.shuffleBtn);
    
    playBtn.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        if( track_index == -1 )
        {
          _set_track( 0 );
          play();
          return;
        }
        
        playpause();
      }
    });  
    
    nextBtn.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        next();
      }
    });   
    
    prevBtn.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        previous(); 
      }
    });
    
    shuffleBtn.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        if( playback_shuffle )
          disableShuffle();
        else
          enableShuffle();
      }
    });    
  }
  
  final public void pause()
  {
    Intent dbmAudioServiceIntent = new Intent( this , dbmAudioService.class );
    dbmAudioServiceIntent.setAction(dbmAudioService.ACTION_PAUSE);
    startService(dbmAudioServiceIntent);        
  }
  
  final public dbmAudioFile getCurrentTrack()
  {
    if( playback_shuffle && audio_files_shuffled != null )
      return audio_files_shuffled.get( track_index );
    
    return audio_files.get( track_index );    
  }
  
  final public void play()
  {
    dbmAudioFile af = getCurrentTrack();
    
    if( af == null )
      return;
    
    storeCurrentTrack( track_index );
    
    Intent dbmAudioServiceIntent = new Intent( this , dbmAudioService.class );
    dbmAudioServiceIntent.setAction(dbmAudioService.ACTION_START);
    dbmAudioServiceIntent.putExtra( "com.bluebird.af" , af.getMediaUrl() );
    startService(dbmAudioServiceIntent);    
  }  
  
  final public void playpause()
  {
    Intent dbmAudioServiceIntent = new Intent( this , dbmAudioService.class );
    dbmAudioServiceIntent.setAction(dbmAudioService.ACTION_PLAYPAUSE);
    startService(dbmAudioServiceIntent);        
  }
  
  final public void previous()
  {
    cbtoast("");
    _set_track( track_index - 1 );
    dbmAudioFile af = getCurrentTrack();
    
    if( af == null )
      return;
    
    load_play( af );
  }
  
  final public void next()
  {
    cbtoast("");
    _set_track( track_index + 1 );
    dbmAudioFile af = getCurrentTrack();
    
    if( af == null )
      return;
    
    load_play( af );
  }
	
	private void _load_list()
	{
	  // no net connection
	  if(!inetEnabled())
	  {
	    showToast("Unable to refresh list without a Net connection");
	    return;
	  }
	  
	  cbtoast("Loading list...");
	  new loadListTask().execute();
	}
	
	private void _populate_list()
	{
	  // no files found
	  if( audio_files.size() < 1 )
	  {
      showToast("There were no audio files found.");
	    return;
	  }
	  
	  audio_grid.setAdapter(new AudioFileAdapter(this , audio_files));

	  audio_grid.setOnItemClickListener(new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
          
          dbmAudioFile af = audio_files.get(position);
          
          if( af == null )
            return;
          
          _set_track( position );
          load_play( af );
        }
    });	  
	}
	
	private void _set_track( Integer newTrackIndex )
	{
    if( newTrackIndex < 0 )
      newTrackIndex = audio_files.size() - 1;
      
    if( newTrackIndex > (audio_files.size() - 1))
      newTrackIndex = 0;
      
    track_index = newTrackIndex;
    return;
	}
	
	public void load_play( dbmAudioFile af )
	{
    if( af.validMediaUrl() )
    {
      Log.e( LOG_TAG , "load_play | media url available" );
      play();
      return;
    }
    
    Log.e( LOG_TAG ,"load_play API media query | " + af.getFilePath());
    
    cbtoast( "Loading track..." );
    new loadMediaUrlTask().execute( af );
	}
	
  // set the current track display in the control bar
	public void cbtoastTrack()
	{
    dbmAudioFile af = getCurrentTrack();	  
    String filepath = af.getFilePath();
    Integer last_slash = filepath.lastIndexOf("/");
    filepath = filepath.substring( last_slash + 1 );
    filepath = filepath.substring( 0 , filepath.lastIndexOf(".") );
    cbtoast( filepath );    
	}
	
	public void cbtoast( String m )
	{
	  ct.setText( (CharSequence) m );
	}
	
	public void enableShuffle()
	{
	  playback_shuffle = true;
	  
	  if( audio_files == null || audio_files.size() < 2 )
	    return;
	  
	  try{
	  // mStrings = listItems.toArray(new String[listItems.size()]);  
	    
	  dbmAudioFile[] a = audio_files.toArray( new dbmAudioFile[audio_files.size()] );
	  audio_files_shuffled = new ArrayList<dbmAudioFile>( Arrays.asList( fisherYatesShuffle( a ) ) );
	  }catch(Exception e){
	    Log.e( LOG_TAG , "shuffle failed" , e );
	  }
	  cbtoast("Shuffle On");
	}
	
	public void disableShuffle()
	{
	  playback_shuffle = false;
	  audio_files_shuffled = null;
	  cbtoast("Shuffle Off");
	}
	
  // Implementing Fisher�Yates shuffle
  private static dbmAudioFile[] fisherYatesShuffle(dbmAudioFile[] ar)
  {
    Random rnd = new Random();
    for (int i = ar.length - 1; i >= 0; i--)
    {
      int index = rnd.nextInt(i + 1);
      // Simple swap
      dbmAudioFile a = ar[index];
      ar[index] = ar[i];
      ar[i] = a;
    }
    
    return ar;
  }	
	
  private String[] getKeys() 
  {
      SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
      String key = prefs.getString(ACCESS_KEY_NAME, null);
      String secret = prefs.getString(ACCESS_SECRET_NAME, null);
      
      if (key != null && secret != null) {
      	String[] ret = new String[2];
      	ret[0] = key;
       	ret[1] = secret;
       	return ret;
      } else {
       	return null;
      }
  }
  
  private Integer getStoredTrack()
  {
    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
    return prefs.getInt("current_track" , 0 );
  }
  
  private void storeCurrentTrack( Integer af_index )
  {
    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
    Editor edit = prefs.edit();
    edit.putInt( "current_track" , af_index );
    edit.commit();    
  }
  
  // Save the access key for later
  private void storeKeys(String key, String secret) 
  {
    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
    Editor edit = prefs.edit();
    edit.putString(ACCESS_KEY_NAME, key);
    edit.putString(ACCESS_SECRET_NAME, secret);
    edit.commit();
  }
  
  private void storeAudioFiles()
  {
    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
    Editor edit = prefs.edit();
    
    edit.commit();
  }
  
  // http://stackoverflow.com/questions/4238921/android-detect-whether-there-is-an-internet-connection-available
  private boolean inetEnabled() 
  {
    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null;
  }
  
  private void signOut() 
  {
    // invalidate session credentials
    mApi.getSession().unlink();

    // remove stored keys
    clearKeys();
  }  
  
  private void clearKeys() 
  {
    SharedPreferences prefs = getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
    Editor edit = prefs.edit();
    edit.clear();
    edit.commit();
  }
  
  private class loadListTask extends AsyncTask<Void,Void,Integer>
  {
    @Override
    protected Integer doInBackground(Void...params) 
    {
      try {
        Entry entry = mApi.metadata( "/." , 0 , null, true , null );
        List<Entry> contents = entry.contents;
        dbmAudioFile af;
        audio_files = new ArrayList<dbmAudioFile>();
          
        if (contents != null) 
        {
          for (Entry ent:contents) 
          {
            if( ent.isDir )
                continue;
              
            if( !dbmAudioService.file_supported( ent.fileName() ))
              continue;
            
            // add to list
            af = new dbmAudioFile();
            af.setFilePath( ent.path );
            audio_files.add( af );
          }
        }     
      } catch (DropboxException e) {
        Log.e("Db API Log", "metadata call failed" , e);
      }
      
      return 0;
    }

    @Override
    protected void onProgressUpdate(Void...Progress) 
    {
    }

    @Override
    protected void onPostExecute(Integer result) 
    {
      _populate_list();      
      cbtoast("Ready");
    }
  }  
  
  private class loadMediaUrlTask extends AsyncTask<dbmAudioFile,Void,Integer>
  {
    @Override
    protected Integer doInBackground(dbmAudioFile...params) 
    {
      try {
        DropboxLink dbl = mApi.media( params[0].getFilePath() , false );
        params[0].setMediaUrl( dbl.url , dbl.expires );
        Log.e("Db sys" , "setting media url "+dbl.url);
      } catch (DropboxException e) {
        Log.e("Db API Log", "media call failed" , e);
      }
      
      return 0;
    }

    @Override
    protected void onProgressUpdate(Void...Progress) 
    {
    }

    @Override
    protected void onPostExecute(Integer result) 
    {
      play();
    }
  }
  
  private void _load_receiver()
  {
    dbmRadio = new dbmIntentReceiver();
    IntentFilter inft;
    Context ct = getApplicationContext();
    
    inft = new IntentFilter(dbmAudioService.UPDATE_TRACK_PREPARE);
    ct.registerReceiver( dbmRadio , inft );
    
    inft = new IntentFilter(dbmAudioService.UPDATE_DIE);
    ct.registerReceiver( dbmRadio , inft );
    
    inft = new IntentFilter(dbmAudioService.UPDATE_TRACK_COMPLETE);
    ct.registerReceiver( dbmRadio , inft );    
    
    inft = new IntentFilter(dbmAudioService.UPDATE_PLAYER_ERROR);
    ct.registerReceiver( dbmRadio , inft );    
    
    inft = new IntentFilter(dbmAudioService.ACTION_STOP);
    ct.registerReceiver( dbmRadio , inft );    
    
    inft = new IntentFilter(dbmAudioService.ACTION_START);
    ct.registerReceiver( dbmRadio , inft );    
    
    inft = new IntentFilter(dbmAudioService.ACTION_PAUSE);
    ct.registerReceiver( dbmRadio , inft );    
    
    inft = new IntentFilter(dbmAudioService.ACTION_PLAYPAUSE);
    ct.registerReceiver( dbmRadio , inft );
    
    inft = new IntentFilter(dbmAudioService.ACTION_PAUSED_STOP);
    ct.registerReceiver( dbmRadio , inft );        
    
    inft = new IntentFilter(android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY);
    ct.registerReceiver( dbmRadio , inft );
    
    inft = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);
    ct.registerReceiver( dbmRadio , inft );
  }
  
  private class dbmIntentReceiver extends BroadcastReceiver 
  {
    @Override
    public void onReceive(Context context, Intent intent) 
    {
      Log.e("Db sys","onReceive: "+String.valueOf(intent.getAction()));
      String action = intent.getAction();
      
      if( action.equals( dbmAudioService.UPDATE_TRACK_PREPARE ))
      {
        cbtoastTrack();
        return;
      }

      if( action.equals( dbmAudioService.UPDATE_TRACK_COMPLETE ))
      {
        next();
        return;
      }      
      
      if (action.equals(android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY)) 
      {
        pause();
        return;
      }
      
      if(action.equals(Intent.ACTION_MEDIA_BUTTON)) 
      {
        KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
        
        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
          return;

        switch (keyEvent.getKeyCode()) {
          case KeyEvent.KEYCODE_HEADSETHOOK:
          case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
            playpause();
          break;
          /*  
          case KeyEvent.KEYCODE_MEDIA_PLAY:
              context.startService(new Intent(MusicService.ACTION_PLAY));
          break;
          case KeyEvent.KEYCODE_MEDIA_PAUSE:
              context.startService(new Intent(MusicService.ACTION_PAUSE));
          break;
          */
          case KeyEvent.KEYCODE_MEDIA_STOP:
            pause();
          break;
          case KeyEvent.KEYCODE_MEDIA_NEXT:
            next();
          break;
          case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
            previous();
          break;
        }
        
        return;
      }
    }
  } 
    
  private void showToast(String msg)
  {
    Toast error = Toast.makeText(this, msg, Toast.LENGTH_LONG);
    error.show();
  }
}

