
package com.bluebird;

import java.lang.System;
import java.util.Date;
import android.util.Log;
import android.os.Parcelable;
import android.os.Parcel;

public class dbmAudioFile implements Parcelable
{
  private String file_path = "";
  private String media_url = "";
  private Long url_expires;
  
  public dbmAudioFile()
  {
  }
  
  public dbmAudioFile( Parcel in )
  {
  }
  
  final public void setFilePath( String f )
  {
    file_path = f;
  }
  
  final public String getFilePath()
  {
    return file_path; 
  }
  
  final public String getMediaUrl()
  {
    return media_url; 
  }
  
  final public void setMediaUrl( String url , Date exp )
  {
    media_url = url;
    url_expires = exp.getTime();
  }
  
  final public boolean validMediaUrl()
  {
    if( media_url.length() < 1 )
      return false;
    
    return (System.currentTimeMillis()/1000) < url_expires;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel out, int flags) {
    out.writeString(media_url);
    out.writeString(file_path);
    out.writeLong(url_expires);
  }
  
  public static final Parcelable.Creator<dbmAudioFile> CREATOR
          = new Parcelable.Creator<dbmAudioFile>() {
      public dbmAudioFile createFromParcel(Parcel in) {
          return new dbmAudioFile(in);
      }
  
      public dbmAudioFile[] newArray(int size) {
          return new dbmAudioFile[size];
      }
  };
}