# Overview #

Android audio player that uses the Dropbox media API. Browse your Dropbox folders for music and then stream directly from the Web, rather than first downloading files or doing any syncing.
